import React from "react";
import "antd/lib/grid/style/css";
import { Row, Col } from "antd";

import "./searchResult.css";
import ProductImage from "../productImage/productImage";

function SearchResult(props) {
  function makeNewList() {
    //make a list of products to display if filters are used.
    // Used .filter method to better show the action, and .some to stop looping if atleast one of the features are discoverd. This prevents memory leaks of pointless loops if required condition already was met.
    return props.products.filter((elem, index, arr) => {
      return elem.features.some(feature => {
        let searchFilter = props.filters.get(feature.name);
        return feature.values.some(val => {
          //console.log(elem.name, searchFilter, val);
          return searchFilter.selected.indexOf(val) !== -1 ? true : false;
        });
      });
    });
  }

  //check if filters status is true, which means they are used. Else just show all products
  const productsList = props.status ? makeNewList() : props.products;

  return (
    <div className="searchResult">
      <Row>
        {productsList.map((product, prodZ) => {
          //console.log(JSON.stringify(product));
          return (
            <Col span={6} key={prodZ}>
              {product.name}
              <ProductImage product={product} />
            </Col>
          );
        })}
      </Row>
    </div>
  );
}

export default SearchResult;

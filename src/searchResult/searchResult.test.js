import React from "react";
import ReactDOM from "react-dom";
import renderer from "react-test-renderer";
import SearchResult from "./searchResult";

//change it() to it.only() to speed up testing dev
it("SearchResult Test", () => {
  const filters = [
    [
      "colour",
      {
        all: ["black", "white", "sliver", "red", "pink", "gray", "blue"],
        selected: []
      }
    ],
    ["size", { all: ["S", "M", "L", "XL"], selected: [] }],
    [
      "style",
      {
        all: ["bomber", "zipup jacket", "sweater", "zipper sweater"],
        selected: []
      }
    ]
  ];
  const products = [
    {
      name: "uno mucho",
      img1: "2400M_61_a.jpg",
      img2: "2400M_639.jpg",
      features: [
        {
          name: "colour",
          values: ["black", "white"]
        },
        {
          name: "size",
          values: ["S", "M", "L"]
        },
        {
          name: "style",
          values: ["bomber"]
        }
      ]
    },
    {
      name: "blingy",
      img1: "2703M_539.jpg",
      img2: "2703M_706_a.jpg",
      features: [
        {
          name: "colour",
          values: ["sliver", "black"]
        },
        {
          name: "size",
          values: ["L", "XL"]
        },
        {
          name: "style",
          values: ["zipup jacket"]
        }
      ]
    }
  ];
  const tree = renderer
    .create(
      <SearchResult filters={filters} status={true} products={products} />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

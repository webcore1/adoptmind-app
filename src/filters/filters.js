import React from "react";
import "antd/lib/collapse/style/css";
import "./filters.css";
import { Collapse } from "antd";
const Panel = Collapse.Panel;

function Filters(props) {
  return (
    <Collapse className="filters" bordered={false} defaultActiveKey={["1"]}>
      {Array.from(props.filters, ([key, items]) => {
        //console.log(key, items);
        return (
          <Panel key={key} header={key} style={{ textTransform: "uppercase" }}>
            <ul>
              {items.all.map((item, iz) => {
                let status = items.selected.some(itemSelected => {
                  return itemSelected === item;
                })
                  ? "selected"
                  : "";

                return (
                  <li
                    className={status}
                    style={{
                      listStyleType: "none",
                      textTransform: "capitalize"
                    }}
                    key={iz}
                  >
                    <a onClick={e => props.handleClick([key, item], e)}>
                      {item}
                    </a>
                  </li>
                );
              })}
            </ul>
          </Panel>
        );
      })}
    </Collapse>
  );
}
export default Filters;

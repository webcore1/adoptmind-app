import React from "react";
import ReactDOM from "react-dom";
import renderer from "react-test-renderer";
import Filters from "./filters";

//change it() to it.only() to speed up testing dev
it("Filters Test", () => {
  const filters = [
    [
      "colour",
      {
        all: ["black", "white", "sliver", "red", "pink", "gray", "blue"],
        selected: []
      }
    ],
    ["size", { all: ["S", "M", "L", "XL"], selected: [] }],
    [
      "style",
      {
        all: ["bomber", "zipup jacket", "sweater", "zipper sweater"],
        selected: []
      }
    ]
  ];
  const tree = renderer
    .create(<Filters filters={filters} handleClick={function() {}} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

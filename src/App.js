import "babel-polyfill";
import React, { Component } from "react";
import logo from "./logo.svg";
import { Layout } from "antd";
import "./App.css";
import Filters from "./filters/filters";
import SearchResult from "./searchResult/searchResult";
import "antd/lib/layout/style/css";

const { Header, Footer, Sider, Content } = Layout;

class App extends Component {
  constructor() {
    super();
    this.state = {
      filters: [],
      products: []
    };
  }

  filterModel(allItems, selectedItems) {
    return { all: allItems, selected: selectedItems };
  }

  makeFilters() {
    //function that makes up the filters model by joining all the filters and values from products list
    let filtersData = new Map();
    this.state.products.forEach(prod => {
      prod.features.forEach(fItem => {
        let filter = filtersData.get(fItem.name);
        if (filter) {
          fItem.values.forEach(value => {
            filter.all.indexOf(value) === -1 && filter.all.push(value);
          });
        } else {
          filtersData.set(fItem.name, this.filterModel(fItem.values, []));
          return true;
        }
      });
    });

    // console.log(filtersData, JSON.stringify(filtersData));

    this.setState({
      filters: filtersData,
      filterStatus: false
    });
  }

  handleFilterChange = (item, e) => {
    e.preventDefault();
    let key = item[0];
    let value = item[1];
    let filtersData = this.state.filters;
    let filter = filtersData.get(key);
    filter.selected.indexOf(value) === -1
      ? filter.selected.push(value)
      : filter.selected.pop(value);
    //console.log(item, filter);

    //updated filter with selected value
    filtersData.set(key, filter);

    this.setState({
      filters: filtersData,
      filterStatus: true
    });
  };

  componentDidUpdate(prevPros, prevState) {
    prevState.products !== this.state.products && this.makeFilters();
  }

  componentDidMount() {
    fetch("/mock-can-goose-products.json", {
      method: "GET",
      headers: {
        Accept: "application/json",
        ContentType: "application/json"
      }
    })
      .then(response => response.json())
      .then(res => {
        this.setState({
          products: res
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    return (
      <div className="App">
        <Layout>
          <Header style={{ background: "white" }}>
            <img alt={"Canada Goose Logo"} src={logo} />
          </Header>
          <Layout>
            <Sider style={{ background: "white" }}>
              <Filters
                filters={this.state.filters}
                handleClick={this.handleFilterChange}
              />
            </Sider>
            <Content style={{ background: "white" }}>
              <SearchResult
                filters={this.state.filters}
                status={this.state.filterStatus}
                products={this.state.products}
              />
            </Content>
          </Layout>
          <Footer style={{ background: "white" }}>Footer</Footer>
        </Layout>
      </div>
    );
  }
}
export default App;

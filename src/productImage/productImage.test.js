import React from "react";
import ReactDOM from "react-dom";
import renderer from "react-test-renderer";
import ProductImage from "./productImage";

//change it() to it.only() to speed up testing dev
it("ProductImage Test", () => {
  const tree = renderer
    .create(
      <ProductImage
        product={{
          name: "zipup",
          img1: "6930M_67.jpg",
          img2: "6930M_699_a.jpg",
          features: [
            { name: "colour", values: ["gray", "blue"] },
            { name: "size", values: ["S", "L", "XL"] },
            { name: "style", values: ["zipper sweater"] }
          ]
        }}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

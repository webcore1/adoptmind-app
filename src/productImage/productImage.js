import React, { Component } from "react";
import "./productImage.css";

class ProductImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hover: false
    };
  }
  //binded methods to change state.
  mouseOver = () => {
    this.setState({ hover: true });
  };
  mouseOut = () => {
    this.setState({ hover: false });
  };
  render() {
    //change img on hover state change
    const img = this.state.hover
      ? this.props.product.img1
      : this.props.product.img2;
    return (
      <div
        onMouseOver={this.mouseOver.bind(this)}
        onMouseOut={this.mouseOut.bind(this)}
      >
        <img alt={this.props.name} src={img} style={{ maxHeight: "400px" }} />
      </div>
    );
  }
}
export default ProductImage;

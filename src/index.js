import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";

//const { registerObserver } = require("react-perf-devtool");

ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
//registerObserver();

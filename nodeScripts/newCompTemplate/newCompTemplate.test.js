import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import NewCompTemplate from "./newCompTemplate";

//change it() to it.only() to speed up testing dev
it('NewCompTemplate Test', () => {
    const tree = renderer
        .create(<NewCompTemplate />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});

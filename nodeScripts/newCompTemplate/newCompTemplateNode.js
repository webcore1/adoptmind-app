var fs = require('fs');
var compFolder = "../../src/comps/";
//var compFolder = "../../src/room/";

var mkdirp = require('mkdirp');
var fs = require('fs');
var getDirName = require('path').dirname;
var newCompTemplateName = "NewCompTemplate";

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function lowercaseFirstLetter(string) {
    return string.charAt(0).toLowerCase() + string.slice(1);
}

//check new comp has a name
if (!process.argv[2]) {
    console.log('Give your new comp a name!');
    process.exit(1);
} else {
    var newCompName = process.argv[2];
    console.log('Making a new: ' + newCompName + ' Comp');
}

var compName = newCompName;
var path = compFolder + compName + "/";

function writeFile(path, contents, cb) {
    mkdirp(getDirName(path), function (err) {
        if (err) return cb(err);
        fs.writeFile(path, contents, cb);
    });
}

function copyTemp(filename, ext) {
    fs.readFile(filename, 'utf8', function (err, data) {
        if (err) throw err;
        console.log('OK: ' + filename);
        console.log(data)

        //toUpperCase
        data = data.replace(new RegExp(capitalizeFirstLetter(newCompTemplateName), 'g'), capitalizeFirstLetter(newCompName)
        );
        //toLowerCase
        data = data.replace(new RegExp(lowercaseFirstLetter(newCompTemplateName), 'g'), lowercaseFirstLetter(newCompName)
        );

        makeCompFile(data, ext);
        //search replace NewCompTemplate

    });
}

function makeCompFile(data, ext) {
    var file = path + compName + ext;
    writeFile(file, data, (error) => { /* handle error */ });
    console.log("File created: " + file);
}

var jsTemplate = "./newCompTemplate.js";
var cssTemplate = "./newCompTemplate.css";
var testTemplate = "./newCompTemplate.test.js";

copyTemp(jsTemplate, ".js");
copyTemp(cssTemplate, ".css");
copyTemp(testTemplate, ".test.js");
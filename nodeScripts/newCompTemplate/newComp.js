const fs = require("fs");
const path = require("path");
const mkdirp = require("mkdirp");

// Setting up relative paths
const appDirectory = fs.realpathSync(process.cwd());
const resolvePath = relativePath => path.resolve(appDirectory, relativePath);

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function lowercaseFirstLetter(string) {
  return string.charAt(0).toLowerCase() + string.slice(1);
}

let componentName = "";
let componentBaseFolder = "src/";

//check new comp has a name
if (!process.argv[2]) {
  console.log("Give your new comp a name!");
  process.exit(1);
} else {
  componentName = process.argv[2];
  console.log("Making a new: " + componentName + " Component");
}

if (process.argv[3]) {
  componentBaseFolder = "src/" + process.argv[3] + "/";
}

let extensions = [".js", ".css", ".test.js"];

scaffoldComponent = (componentBaseFolder, componentName, extensions) => {
  // Create folder first
  mkdirp(resolvePath(componentBaseFolder + componentName), err => {
    if (err) {
      console.log(err);
    } else {
      // Using base templates, copy and edit the reference to component name to provided name
      extensions.map(ext => {
        let baseFile = resolvePath(
          "nodeScripts/newCompTemplate/newCompTemplate" + ext
        );
        let newFile = resolvePath(
          componentBaseFolder + componentName + "/" + componentName + ext
        );

        let baseTemplate = fs.createReadStream(baseFile, "utf8");
        let newTemplate = "";

        // Read base template and replace "newCompTemplate" to provided component name
        baseTemplate.on("data", function (chunk) {
          newTemplate += chunk
            .toString()
            .replace(
            new RegExp(lowercaseFirstLetter("newCompTemplate"), "g"),
            lowercaseFirstLetter(componentName)
            )
            .replace(
            new RegExp(capitalizeFirstLetter("newCompTemplate"), "g"),
            capitalizeFirstLetter(componentName)
            );
        });

        // Create new component template
        baseTemplate.on("end", function () {
          fs.writeFile(newFile, newTemplate, function (err) {
            if (err) {
              return console.log(err);
            } else {
              console.log(`Generated ${componentName + ext} in ${newFile}`);
            }
          });
        });
      });
    }
  });
};

scaffoldComponent(componentBaseFolder, componentName, extensions);
